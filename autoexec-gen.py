import os
from typing import List

def load(file_name: str) -> dict[str, str]:
    """Loads a file and outputs it as string

    Args:
        file_name (str): name of file to load
    """    
    out = {}
    with open(os.path.abspath(file_name), "r") as f:
        for l in f:
            if l.strip() == "":
                continue
            name, *spl = l.strip().split(" ")
            if name == "bind":
                out[name + " " + spl[0]] = spl[1:]
            else:
                out[name] = spl
    return out

def compare(default: dict[str, str], gen: dict[str, str]):
    diffs = []
    used_keys = []
    key_list = list(default.keys())
    key_list.extend(list(generated.keys()))
    
    # print(key_list)
    for key in key_list:
        if key not in default:
            diffs.append((key, generated[key]))
            used_keys.append(key)
        elif key not in gen and key not in used_keys:
            diffs.append((key, default[key]))
            used_keys.append(key)
        elif key not in used_keys and default[key] != gen[key]:
            diffs.append((key, gen[key]))
            used_keys.append(key)
            
    
    return diffs

def store_autoexec(diffs: List[tuple[str, List[str]]]):
    with open("autoexec.cfg", "w") as autoexec:
        for name, value in diffs:
            autoexec.write(name + " " + " ".join(value)+"\n")

if __name__ == "__main__":
    default = load("original.cfg")
    generated = load("config.cfg")
    diffs = compare(default, generated)
    store_autoexec(sorted(diffs, key=lambda a: a[0]))